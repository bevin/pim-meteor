Roles = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		return{
			roles:rolesDB.find().fetch()
		}
	},
	updateRole(role)
	{
		Meteor.call('updateRole',role._id,role.name);
	},
	addRole()
	{
		Meteor.call('addRole','New Role');
	},
	deleteRole(role){
		Meteor.call('deleteRole',role._id);
	},
	render() {
		self=this;
		roles = this.data.roles.map(function(role){
			return(
				<Role key={role._id} role={role} update={self.updateRole} delete={self.deleteRole}/>
			)
		})
		return (
			<div className="roles">
				<div className="ui internally celled grid">
					{roles}
				</div>
				
				<div className="ui internally celled grid">
					<div className="row">
						<div className="three wide column">
							<button className='ui black fluid massive icon button' onClick={this.addRole}>
								<i className='big icon plus'></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
});
