Role = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		return{
			goals:bigGoalsDB.find({roleId:this.props.role._id}).fetch()
		}
	},
	updateGoal(goal){
		Meteor.call('updateGoal',goal._id,goal.text);
	},
	deleteGoal(goal){
		Meteor.call('deleteGoal',goal._id);
	},
	onChange(event){
		newVal = $(event.target).text();
		role = this.props.role;
		role.name = newVal;
		this.props.update(role);
	},
	onDelete(event)
	{
		this.props.delete(this.props.role);
	},
	onEdit(event){
		$(event.target).closest('.table').css('transform','translateX(-50%)');
	},
	onBack(event){
		$(event.target).closest('.table').css('transform','translateX(0%)');
	},
	onAddGoal(event){
		Meteor.call('addGoal',this.props.role._id);
	},
	render(){
		self=this;
		goals = this.data.goals.map(function(goal){
			return(
				<Goal key={goal._id} goal={goal} update={self.updateGoal} delete={self.deleteGoal}/>
			)
		})
		return(
			<div className="row role">
				<div className="three wide column">
					<table className="ui two column table">
						<tbody>
						<tr>
							<td>
								<div className='ui segment'>
									<p>{this.props.role.name}</p>
									<div className='ui black top right attached mini label'>
										<i className='icon edit' onClick={this.onEdit}></i>
										<i className='icon plus' onClick={this.onAddGoal}></i>
									</div>
								</div>
							</td>
							<td>
								<div className='ui segment'>
									<div className='editable' contentEditable onBlur={this.onChange}>
										{this.props.role.name}
									</div>
									<div className='ui black top right attached mini label'>
										<i className='icon reply' onClick={this.onBack}></i>
										<i className='icon trash' onClick={this.onDelete}></i>
									</div>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div className="thirteen wide column">
					{goals}
				</div>
			</div>
		)
	}
});
