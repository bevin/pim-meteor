if(Meteor.isServer)
{
	Meteor.startup(function(){

	});

	Meteor.publish('roles',function(){
		return rolesDB.find({
			owner:this.userId
		});
	});
	Meteor.publish('big-goals',function(){
		return bigGoalsDB.find({
			owner:this.userId
		});
	});
}
Meteor.methods({
	//role methods
	addRole(name){
		if(!Meteor.userId()){
			throw new Meteor.error("not-authorized");
		}

		rolesDB.insert({
			owner:Meteor.userId(),
			name:name
		});
	},
	deleteRole(roleId)
	{
		if(!Meteor.userId()){
			throw new Meteor.error("not-authorized");
		}
		bigGoalsDB.remove({roleId:roleId});
		rolesDB.remove(roleId);
	},
	updateRole(id,name){
		rolesDB.update(id,{$set:{name:name}});
	},
	//goal methods
	addGoal(roleId){
		if(!Meteor.userId()){
			throw new Meteor.error("not-authorized");
		}
		bigGoalsDB.insert({
			owner:Meteor.userId(),
			roleId:roleId
		});
	},
	deleteGoal(id){
		bigGoalsDB.remove(id);
	},
	updateGoal(id,text){
		bigGoalsDB.update(id,{$set:{text:text}});
	},
})
