Goal = React.createClass({
	onChange(event){
		newVal = $(event.target).val();
		goal = this.props.goal;
		goal.text = newVal;
		this.props.update(goal);
	},
	onDelete()
	{
		this.props.delete(this.props.goal);
	},
	componentDidMount(){
		$(this.refs.goal).popup();	
	},
	render(){
		return(
			<div className="goal ui action mini input fluid">
				<input type="text" ref="goal" value={this.props.goal.text} data-content={this.props.goal.text} data-variation="inverted" onChange={this.onChange}/>
				<button className="ui black mini icon button" onClick={this.onDelete}>
					<i className="trash icon"></i>
				</button>
			</div>
		)
	}
});
