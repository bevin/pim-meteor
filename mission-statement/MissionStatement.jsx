MissionStatement = React.createClass({
	getInitialState(){
		return {
			id:null
		}
	},
	componentDidMount(){
		self = this;
		$('#summernote').summernote({
			airMode:true,
			popover:{
				air:[
					['style', ['bold', 'italic', 'underline', 'clear']],
				    ['font', ['strikethrough', 'superscript', 'subscript']],
				    ['fontsize', ['fontsize']],
				    ['color', ['color']],
				    ['para', ['ul', 'ol', 'paragraph']],
				    ['height', ['height']]
				],
			},
			placeholder:'write here...',
			dialogsFade:true,
			callbacks:{
				onBlur(){
					console.log('blur')
					if($('.note-popover').css('display')!='none')
					{
						console.log('skipped');
						return;
					}

					html = $('#summernote').summernote('code');
					if(self.data.statement)
					{
						Meteor.call('updateStatement',self.data.statement._id,html);
					}
					else
					{
						Meteor.call('insertStatement',html);
					}
				}
			}
		});
		if(this.data.statement)
		{
			$('#summernote').summernote('code',this.data.statement.text);
		}
	},
	componentWillUnmount(){
		$('summernote').summernote('destroy');
		$('.note-popover').remove();
	},
	mixins:[ReactMeteorData],
	getMeteorData(){
		return{
			statement:statementDB.findOne()
		}
	},
	render(){
		if(this.data.statement)
		{
			$('#summernote').summernote('code',this.data.statement.text);
		}
		return (
			<div id="summernote"></div>
			);
	}
});
