if(Meteor.isServer)	
{
	Meteor.startup(function(){

	});

	Meteor.publish('mission-statement',function(){
		return statementDB.find({
			owner:this.userId
		});
	});

}
Meteor.methods({
	insertStatement(text){
		if(!Meteor.userId()){
			throw new Meteor.error("not-authorized");
		}

		statementDB.insert({
			owner:Meteor.userId(),
			text:text
		});
	},
	updateStatement(id,text){
		statementDB.update(id,{text:text});
	}
});
