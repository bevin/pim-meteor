//define databases
statementDB = new Mongo.Collection('mission-statement');
rolesDB = new Mongo.Collection('roles');
bigGoalsDB = new Mongo.Collection('big-goals');
sGoalsDB = new Mongo.Collection('weekly-goals');


if(Meteor.isClient)
{
	Meteor.subscribe('mission-statement');
	Meteor.subscribe('roles');
	Meteor.subscribe('big-goals');
	Meteor.subscribe('weekly-goals');

	Accounts.ui.config({
		passwordSignupFields:'USERNAME_ONLY'
	});

}
