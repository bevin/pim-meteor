WeeklyRole = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		return{
			goals:sGoalsDB.find({roleId:this.props.role._id}).fetch()
		}
	},
	updateGoal(goal){
		Meteor.call('updateWeeklyGoal',goal._id,{text:goal.text,priority:goal.priority});
	},
	deleteGoal(goal){
		Meteor.call('deleteWeeklyGoal',goal._id);
	},
	onAddGoal(event){
		Meteor.call('addWeeklyGoal',this.props.role._id,'','b99');
		this.setPriority(null,null);
	},
	setPriority(id,priority)
	{
		this.props.setPriority(id,priority);
	},
	render(){
		self=this;
		goals = this.data.goals.map(function(goal){
			return(
				<WeeklyGoal key={goal._id} goal={goal} update={self.updateGoal} delete={self.deleteGoal} setPriority={self.setPriority}/>
			)
		})
		return(
			<div className="row w-role">
				<div className="column">
					<table className="ui table">
						<tbody>
						<tr>
							<td>
								<div className='ui segment'>
									<p>{this.props.role.name}</p>
									<div className='ui black top right attached mini label'>
										<i className='icon plus' onClick={this.onAddGoal}></i>
									</div>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div className="column">
					{goals}
				</div>
			</div>
		)
	}
});
