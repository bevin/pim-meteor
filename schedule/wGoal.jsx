WeeklyGoal = React.createClass({
	onDragStart(event)
	{
		event.dataTransfer.setData("id",this.props.goal._id);
	},
	onChange(event){
		newVal = $(event.target).val();
		goal = this.props.goal;
		goal.text = newVal;
		this.props.update(goal);
	},
	onDelete()
	{
		this.props.delete(this.props.goal);
	},
	setPriority(id,value)
	{
		this.props.setPriority(id,value);
	},
	onItemChange(event){
		value = $(event.target).closest('.item').attr('data-value');
		goal = this.props.goal;
		newPriority = 0;
		if(value=='-')
		{
			newPriority = Number(goal.priority.substr(1,2))-1;
			if(newPriority<0)
				newPriority = 0;

			goal.priority = goal.priority.substr(0,1)+String(newPriority);
		}
		else if(value=='+')
		{
			newPriority = Number(goal.priority.substr(1,2))+1
			goal.priority = goal.priority.substr(0,1)+String(newPriority);
		}
		else
		{
			//put to last when type changed
			goal.priority = value+'99';
		}
		this.setPriority(this.props.goal._id,goal.priority)
	},
	componentDidMount(){
		$(this.refs.goal).popup();

		$(this.refs.toolbar).children('.dropdown').dropdown();
	},
	render(){
		priorityN = this.props.goal.priority.substr(1,2);
		priorityC = this.props.goal.priority.substr(0,1);
		colors = {'a':'red','b':'violet','c':'green','d':'orange'};
		priorityClass = colors[priorityC];
		return(
			<div className="w-goal ui action mini input fluid">
				<input type="text" ref="goal" value={this.props.goal.text} data-content={this.props.goal.text} data-variation="inverted" onChange={this.onChange} draggable="true" onDragStart={this.onDragStart}/>
				<div ref="toolbar" className={"ui mini top right attached label "+priorityClass}>
					<div className="ui pointing dropdown">
						<div className="priority">{priorityN}</div>
						<div className="menu">
							<div className="black item" data-value='+' onClick={this.onItemChange}><i className="mini add square icon"></i></div>
							<div className="black item" data-value='-' onClick={this.onItemChange}><i className="mini minus square icon"></i></div>
							<div className="red item" data-value='a' onClick={this.onItemChange}>A</div>
							<div className="violet item" data-value='b' onClick={this.onItemChange}>B</div>
							<div className="green item" data-value='c' onClick={this.onItemChange}>C</div>
							<div className="orange item" data-value='d' onClick={this.onItemChange}>D</div>
						</div>
					</div>
					<i className="mini trash icon" onClick={this.onDelete}></i>
				</div>
			</div>
		)
	}
});
