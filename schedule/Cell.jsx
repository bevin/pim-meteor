Cell = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		id = String(this.props.rowId)+'-'+this.props.colId
		return{
			goals:sGoalsDB.find({celIds:{$in:[id]}}).fetch()
		}
	},
	allowDrop(event)
	{
		event.preventDefault();
	},
	onDrop(event)
	{
		event.preventDefault();
		
		id = event.dataTransfer.getData("id");
		goal = sGoalsDB.findOne(id)	
		cells = goal.celIds;
		//if it's a move effect, then delete the original
		if(event.dataTransfer.dropEffect=='move')
		{
			fromCell = event.dataTransfer.getData('fromCell')
			index = cells.indexOf(fromCell);
			if(index > -1)
			{
				cells.splice(index,1);
			}
		}
		//now add new cellId to the array
		cellId = String(this.props.rowId)+'-'+this.props.colId
		if(-1 == goal.celIds.indexOf(cellId))
		{
			cells.push(cellId);
		}
		Meteor.call('updateWeeklyGoal',id,{'celIds':cells});
	},
	onDragStart(event)
	{
		if(event.shiftKey)
		{
			//copy
			event.dataTransfer.effectAllowed = 'copy';
		}
		else
		{
			//move
			cellId = String(this.props.rowId)+'-'+this.props.colId;
			event.dataTransfer.setData("fromCell",cellId);
		}
		id = $(event.target).attr('data-id');
		event.dataTransfer.setData("id",id);
	},
	componentDidMount()
	{
		$(this.refs.cell).find('.label').popup();
	},
	componentDidUpdate(){
		$(this.refs.cell).find('.label').popup();
	},
	closeTask(event)
	{
		//close popup
		$(event.target).parent().popup('hide');
		//remove cell id from id specified goal
		id = $(event.target).attr('data-id');
		cellId = String(this.props.rowId)+'-'+this.props.colId
		goal = sGoalsDB.findOne(id)	
		cells = goal.celIds;
		index = cells.indexOf(cellId);
		if(index > -1)
		{
			cells.splice(index,1);
			Meteor.call('updateWeeklyGoal',id,{'celIds':cells});
		}
	},
	render(){
		self = this;
		goals = this.data.goals.map(function(goal){

			priorityN = goal.priority.substr(1,2);
			priorityC = goal.priority.substr(0,1);
			colors = {'a':'red','b':'violet','c':'green','d':'orange'};
			priorityClass = colors[priorityC];

			text = goal.text.substr(0,1).toUpperCase()+goal.text.substr(1,3).toLowerCase();
			return (
				<div className={"ui mini tag label "+priorityClass} data-id={goal._id} key={goal._id} data-content={goal.text} data-variation="inverted" draggable="true" onDragStart={self.onDragStart} onDragEnd={self.onDragEnd}>
					{text}
					<i className="inverted icon close" data-id={goal._id} onClick={self.closeTask}></i>
				</div>
			)
		});
		return(
			<div className="cell" ref="cell" onDragOver={this.allowDrop} onDrop={this.onDrop}>
				{goals}
			</div>
		)
	}
});
