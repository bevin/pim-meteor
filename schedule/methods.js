if(Meteor.isServer)
{
	Meteor.startup(function(){

	});

	Meteor.publish('weekly-goals',function(){
		return sGoalsDB.find({
			owner:this.userId
		});
	});
}
Meteor.methods({
	//goal methods
	addWeeklyGoal(roleId,text,priority){
		if(!Meteor.userId()){
			throw new Meteor.error("not-authorized");
		}
		sGoalsDB.insert({
			owner:Meteor.userId(),
			roleId:roleId,
			text:text,
			celIds:[],
			priority:priority
		});
	},
	deleteWeeklyGoal(id){
		sGoalsDB.remove(id);
	},
	updateWeeklyGoal(id,obj){
		sGoalsDB.update(id,{$set:obj});
	},
});
