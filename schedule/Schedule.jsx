Schedule = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		return{
			roles:rolesDB.find().fetch(),
		}
	},
	logGoals(goals){
		out = [];
		columnCount = 5;
		rowCount = parseInt(goals.length/columnCount)+1;
		for(var i=0;i<rowCount;i++){
			out[i] = new Array();
		}
		for(var i=0;i<goals.length;i++)
		{
			row = i%rowCount; 
			column = parseInt(i/rowCount);
			out[row][column] = goals[i];
		}
		text = '';
		for(var i=0;i<rowCount;i++)
		{
			for(var j=0;j<columnCount;j++)
			{
				if(out[i][j])
				{
					index = j*rowCount+i;
					text = text + '['+index+']\t'+out[i][j]._id+':'+out[i][j].priority+'\t';
				}
			}
			text += '\n';
		}
		console.log(text);
	},
	setPriority(id,priority)
	{
		goals = sGoalsDB.find().fetch();
		//console.log('raw:');
		//this.logGoals(goals);
		//sort goals by priority
		var reA = /[^a-zA-Z]/g;
		var reN = /[^0-9]/g;
		goals.sort(function(a,b){
			a = a.priority;
			b = b.priority;
		    var aA = a.replace(reA, "");
		    var bA = b.replace(reA, "");
		    if(aA === bA) {
			        var aN = parseInt(a.replace(reN, ""), 10);
			        var bN = parseInt(b.replace(reN, ""), 10);
			        return aN === bN ? 0 : aN > bN ? 1 : -1;
			    } else {
				        return aA > bA ? 1 : -1;
		    }
		});
		//console.log('sorted:');
		//this.logGoals(goals);
		//find and remove the actuator goal from old place
		if(id!=null)
		{
			actuatorIndex = null;
			actuator = null;
			for(var i=0;i<goals.length;i++){
				if(goals[i]._id == id)
				{
					actuatorIndex = i;
					actuator = goals[i];
					goals.splice(i,1);
					break;
				}
			}
			//now insert the actuator to correct place
			previousIndex = -1;
			for(var i=0;i<goals.length;i++){
				//insert actuator once found a bigger priority item
				if(goals[i].priority<priority)
				{
					previousIndex = i;
					continue;
				}
				else
				{
					break;
				}
			}
			//make actuator priority correct, since its type responsible for changing gType later
			actuator.priority = priority;
			goals.splice(previousIndex+1,0,actuator);
		}
		//console.log('correct place:');
		//this.logGoals(goals);

		curType = 'a';
		curIndex = 0;
		for(var i=0;i<goals.length;i++)
		{
			gType = goals[i].priority.substr(0,1);
			gN = goals[i].priority.substr(1,2);
			//change curType while new priority type accurs in the sorted array
			if(gType!=curType)
			{
				curType = gType;
				curIndex = 0;
			}
			//correct priority then save for those
			//	(1)	priority not equal index of the gType type
			//	(2)	id equals the actuator
			if(Number(gN) != curIndex || goals[i]._id == id)
			{
				gN = String(curIndex);
				//whenever the index recalclated, save the item
				value = gType + gN;
				Meteor.call('updateWeeklyGoal',goals[i]._id,{priority:value});
			}
			goals[i].priority = gType + gN;

			curIndex = curIndex+1;
		}

		//console.log('last:');
		//this.logGoals(goals);
		//console.log('input:',id,priority);
	},
	createRow(rowIds)
	{
		rows = rowIds.map(function(rowId){
			columnIds = [0,1,2,3,4,5,6];
			columns = columnIds.map(function(id){
				return (
					<div className="column" key={id}>
						<Cell rowId={rowId} colId={id}/>
					</div>
				)
			}) 
			return (
				<div className="row" key={rowId}>
					{columns}
				</div>
			)
		})	
		return rows;
	},
	render() {
		self=this;
		roles = this.data.roles.map(function(role){
			return(
				<WeeklyRole key={role._id} role={role} setPriority={self.setPriority}/>
			)
		})

		allDayEvents = this.createRow(['p0','p1','p2','p3','p4','p5']);
		appointments = this.createRow(['a8','a9','a10','a11','a12','a13','a14','a15','a16','a17','a18','a19','a20']);
		evenings = this.createRow(['e0','e1','e2','e3','e4','e5']);

		times = [];
		for(var i=8;i<=20;i++)
		{
			str = '0'+String(i)+':00';
			times.push(<span>{str.substr(str.length-5,5)}</span>);
		}

		return (
			<div className="schedule">
				<table className="ui table">
				<tbody>
				<tr>
				<td width="22.2%">
					<div className="ui two column grid">
						{roles}
					</div>
				</td>
				<td width="87.8%">
					<div className="ui seven column center aligned grid">
						<div className="equal width row title">
							<div className="column">Sun</div>
							<div className="column">Mon</div>
							<div className="column">Tue</div>
							<div className="column">Wed</div>
							<div className="column">Thu</div>
							<div className="column">Fri</div>
							<div className="column">Sat</div>
						</div>
						<div className="equal width row title">
							<div className="column">All Day Events</div>
						</div>
						{allDayEvents}
						<div className="equal width row title">
							<div className="column">Appointments/Commitments</div>
							<span className="time-label">
							{times}
							</span>
						</div>
						{appointments}
						<div className="equal width row title">
							<div className="column">Evenings</div>
						</div>
						{evenings}
					</div>
				</td>
				</tr>
				</tbody>
				</table>
			</div>
		);
	}
});
